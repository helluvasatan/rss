//
//  RSSXMLParser.h
//  Rss
//
//  Created by Mikhail Brel on 15/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSDelegate.h"


@interface RSSXMLParser : NSObject

-(id) init;
-(void) parseFromData: (NSData *) data toDelegate: (id<RSSDelegate>) delegate;

@end
