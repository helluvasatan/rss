//
//  RSS.h
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSChannel.h"
#import "RSSDelegate.h"

@interface RSS : NSObject

+(void) loadFromFile: (NSString *) fileName target: (id<RSSDelegate>) receiver;

@end
