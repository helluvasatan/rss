//
//  RSSItem.h
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSSItem : NSObject

-(id) initWithTitle: (NSString *)title
               link: (NSURL *)link
        description: (NSString *)description
            andDate: (NSDate *)date;

@property (readonly, copy) NSString *title;
@property (readonly, retain) NSURL *link;
@property (readonly, copy) NSString *description;
@property (readonly, retain) NSDate *date;

@end
