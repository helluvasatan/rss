//
//  RSSXMLParser.m
//  Rss
//
//  Created by Mikhail Brel on 15/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "RSSXMLParser.h"
#import "RSSItem.h"
#import "RSSChannel.h"
#import "RSSDelegate.h"
#import "DateUtility.h"

const NSInteger ST_INIT         = 0;
const NSInteger ST_ITEM_TITLE   = 1;
const NSInteger ST_ITEM_LINK    = 2;
const NSInteger ST_ITEM_DESCR   = 3;
const NSInteger ST_ITEM_DATE    = 4;

@interface RSSXMLParser() <NSXMLParserDelegate> {
}

@end

@implementation RSSXMLParser {
    NSInteger _state;
    NSString *_title;
    NSURL *_link;
    NSString *_description;
    NSDate *_date;
    
    DateUtility *_dutil;
    NSMutableArray *_array;
    id<RSSDelegate> _delegate;
}

-(id) init
{
    self = [super init];
    if(self) {
        _state = ST_INIT;
        _dutil = [[DateUtility alloc] init];
        _array = [[NSMutableArray alloc] init];
    }
    return self;
}

-(void) dealloc
{
    [_array release];
    [_dutil release];
    
    [super dealloc];
}

-(void) parseFromData: (NSData *) data toDelegate: (id<RSSDelegate>) delegate
{
    NSXMLParser *parser = [[[NSXMLParser alloc] initWithData:data] autorelease];
    parser.delegate = self;
    _delegate = delegate;
    [parser parse];
}

#pragma mark -- NSXMLParserDelegate

-(void) parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    _state = ST_INIT;
    if([elementName isEqualToString:@"item"]) {
        RSSItem *item = [[[RSSItem alloc]initWithTitle:_title link:_link description:_description andDate:_date] autorelease];
        [_array addObject:item];

        [_title release];
        [_date release];
        [_link release];
        [_description release];
    }
    else if ([elementName isEqualToString:@"rss"]) {
        RSSChannel *channel = [[[RSSChannel alloc] initWithTitle:@"ololo" andItems:_array] autorelease];
        [_delegate didRSSLoadSuccess:channel];
    }
}

-(void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:@"title"]) {
        _state = ST_ITEM_TITLE;
    } else if ([elementName isEqualToString:@"link"]) {
        _state = ST_ITEM_LINK;
    } else if ([elementName isEqualToString:@"pubDate"]) {
        _state = ST_ITEM_DATE;
    } else if ([elementName isEqualToString:@"description"]) {
        _state = ST_ITEM_DESCR;
    }
}

-(void) parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(_state == ST_ITEM_TITLE) {
        _title = [string copy];
    }
    else if(_state == ST_ITEM_LINK) {
        _link = [[NSURL URLWithString:string] retain];
    }
    else if(_state == ST_ITEM_DATE) {
        _date = [[_dutil dateWithString: string] retain];
    }
    else if(_state == ST_ITEM_DESCR) {
        _description = [string copy];
    }
}

@end
