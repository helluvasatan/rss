//
//  RSSChannel.m
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "RSSChannel.h"

@implementation RSSChannel{
    NSString *p_title;
    NSArray *p_items;
}

@synthesize title = p_title;

-(id) initWithTitle: (NSString *) title andItems: (NSArray *) items
{
    self = [super init];
    if(self){
        p_title = [title copy];
        p_items = [items copy];
    }
    return self;
}

-(NSArray *) getItems
{
    return p_items;
}

-(void) dealloc
{
    [p_title release];
    [p_items release];
    [super dealloc];
}

@end
