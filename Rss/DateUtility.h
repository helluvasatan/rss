//
//  DateUtility.h
//  Rss
//
//  Created by Mikhail Brel on 16/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateUtility : NSObject

- (id) init;

- (NSDate *) dateWithString: (NSString *) string;

@end
