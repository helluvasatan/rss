//
//  MBViewController.h
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSSDelegate.h"

@interface MBViewController : UIViewController<UITableViewDataSource, UITableViewDelegate, RSSDelegate>
- (IBAction)wannaRssTouchDown:(id)sender;
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath;
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
@property (retain, nonatomic) IBOutlet UITableView *tblRSSItems;

@end
