//
//  MBAppDelegate.h
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MBAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
