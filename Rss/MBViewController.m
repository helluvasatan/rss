//
//  MBViewController.m
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "MBViewController.h"
#import "RSS.h"

@interface MBViewController() {
    NSArray *items;
}

@end

@implementation MBViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"StateCell";
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        [cell autorelease];
    }
    RSSItem *item = [items objectAtIndex:indexPath.row];
    cell.textLabel.text = [NSString stringWithFormat:@"%@;%@;%@", item.title, item.link, item.date] ;
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(!items)
        return 0;
    return [items count];
}

- (IBAction) wannaRssTouchDown:(id)sender
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"rss" ofType:@"xml"];
    [RSS loadFromFile: path target:self];
}

-(void) didRSSLoadSuccess:(id)channel
{
    [items release];
    items = [channel getItems];
    [items retain];
    [[self tblRSSItems] reloadData];
}

-(void) didRSSLoadFailed:(NSError *)error
{
    NSLog(@"Error: %@", error);
    [items release];
    [[self tblRSSItems] reloadData];
}

- (void) dealloc {
    [_tblRSSItems release];
    [items release];
    [super dealloc];
}
@end
