//
//  RSSChannel.h
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSItem.h"

@interface RSSChannel : NSObject

@property (copy, readonly) NSString *title;

-(id) initWithTitle: (NSString *) title andItems: (NSArray *)items;
-(NSArray *) getItems;

@end
