//
//  main.m
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([MBAppDelegate class]));
    }
}
