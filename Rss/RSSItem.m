//
//  RSSItem.m
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "RSSItem.h"

@implementation RSSItem {
    NSString    *p_title;
    NSURL       *p_link;
    NSString    *p_description;
    NSDate      *p_date;
}

@synthesize title = p_title;
@synthesize link = p_link;
@synthesize description = p_description;
@synthesize date = p_date;

-(id) initWithTitle: (NSString *) title
               link: (NSURL *) link
        description: (NSString *) description
            andDate: (NSDate *) date
{
    self = [super init];
    if(self) {
        p_title = [title copy];
        p_link = [link retain];
        p_description = [description copy];
        p_date = [date retain];
    }
    return self;
}

-(void) dealloc
{
    [p_title release];
    [p_link release];
    [p_description release];
    [p_date release];
    [super dealloc];
}
@end
