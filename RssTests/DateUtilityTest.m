//
//  DateUtilityTest.m
//  Rss
//
//  Created by Mikhail Brel on 16/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "DateUtility.h"

@interface DateUtilityTest : XCTestCase

@end

@implementation DateUtilityTest

- (void)setUp
{
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown
{
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample
{
    DateUtility *dutil = [[DateUtility alloc] init];
    NSDate *d = [dutil dateWithString:@"Mon, 23 Jun 2014 21:45:20 +0400"];
    XCTAssertNotNil(d);
}

@end
