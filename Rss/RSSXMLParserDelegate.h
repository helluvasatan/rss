//
//  RSSXMLParserDelegate.h
//  Rss
//
//  Created by Mikhail Brel on 15/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RSSChannel.h"

@protocol RSSXMLParserDelegate <NSObject>

-(void) didRSSParseSuccess: (RSSChannel *) channel;
-(void) didRSSParseFailed: (NSError *) error;

@end
