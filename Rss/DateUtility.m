//
//  DateUtility.m
//  Rss
//
//  Created by Mikhail Brel on 16/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "DateUtility.h"

@implementation DateUtility {
    NSDateFormatter *_dateFormatter;
}

- (id) init
{
    self = [super init];
    if (self) {
        _dateFormatter = [[NSDateFormatter alloc] init];
        [_dateFormatter setDateFormat:@"EEE, dd MMM YYYY HH:mm:ss Z"];
    }
    return self;
}

-(void) dealloc
{
    [_dateFormatter release];
    [super dealloc];
}

- (NSDate *) dateWithString: (NSString *) string
{
    return [_dateFormatter dateFromString: string];
}

@end
