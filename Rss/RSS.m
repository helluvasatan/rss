//
//  RSS.m
//  Rss
//
//  Created by Mikhail Brel on 10/07/14.
//  Copyright (c) 2014 Mikhail Brel. All rights reserved.
//

#import "RSS.h"
#import "RSSXMLParser.h"

@implementation RSS

+(void) loadFromFile: (NSString *) fileName target: (id<RSSDelegate>) receiver
{
    RSSXMLParser *rssParser = [[[RSSXMLParser alloc] init] autorelease];
    NSData *data = [NSData dataWithContentsOfFile:fileName] ;
    [rssParser parseFromData:data toDelegate:receiver];
}
@end
